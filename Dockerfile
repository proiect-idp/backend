FROM node:14-alpine

WORKDIR .

COPY package*.json ./

RUN ["npm", "i"]

COPY . .

RUN chmod 777 ./script.sh

EXPOSE 3000

CMD ["./script.sh"]
